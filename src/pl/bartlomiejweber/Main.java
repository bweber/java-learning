package pl.bartlomiejweber;

import java.io.File;

public class Main {

    public static void main(String[] args) {

        String name;
        int liczba;
        float liczbaZmiennoPrzecinkowa;
        double liczbaZmiennoPrzecinkowa2;
        char literka = 'f';

        name = "Bartek";
        liczbaZmiennoPrzecinkowa = 1.0f;
        liczbaZmiennoPrzecinkowa2 = 2.0;

        System.out.println("Hello World");
        System.out.println(name);

        name = "Ola";
        System.out.print(name + "\t");
        System.out.println(name);

        Car samochod = new Car("VW", 5, "Golf V");
        samochod.printCarDetails();

        String randomName = samochod.getBrand();

        System.out.println(randomName);

        Car samochod2 = new Car("Opel", "Astra");
        samochod2.printCarDetails();

        Car samochod3 = new Car();
        samochod3.printCarDetails();

        File zdjecie = samochod3.getObrazek();
        System.out.println(zdjecie.getPath());

//        Car samochod4 = new Car("Opel");
    }
}
