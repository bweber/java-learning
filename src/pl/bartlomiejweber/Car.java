package pl.bartlomiejweber;

import java.io.File;

/**
 * Created by bweber on 05.05.2017.
 */
public class Car {
    private String brand;
    private int doors;
    private String model;
    private File obrazek;

    public Car(String brand, int doors, String model) {
        this.brand = brand;
        this.doors = doors;
        this.model = model;
    }

    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
        this.doors = 4;
    }

    public Car() {
        this.brand = "Renault";
        this.model = "Laguna II";
        this.doors = 5;
        this.obrazek = new File("C:/obrazek.png");
    }

    public Car(String brand) {
        this.brand = brand;
    }

    public void printCarDetails() {
        System.out.println("My car brand is " + brand + " model: " + model + " with: " + doors + " doors ");
    }

    public String getBrand() {
        return brand;
    }

    public File getObrazek() {
        return obrazek;
    }
}
